<?php
require("db_credentials.php");
?>
<?php
if (isset($_GET["id"]) == FALSE)
{
  header("Location: " . "view-routes.php");
  exit();
}

$id = $_GET["id"];

$connection = connect();

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    $results = showBuses();
    $person = mysqli_fetch_assoc($results);
}
elseif ($_SERVER["REQUEST_METHOD"] == "POST")
{
    updateBusInformation();
}
?>

<?php
  include("headr.php");
?>

<div class="top">
  <h1>UPDATE BUS INFO</h1>
</div>
</header>
<style>

  .top
  {
     position: relative;
     text-align: center;
     color: #767676;
     top: 20px;
  }

  .top h1
  {
     font-size: 52px;
     margin: 25px;
     padding-bottom: 10px;
     font-weight: bold;
     opacity: 0.5;
     line-height: 120%;
     text-decoration: underline;
     text-shadow: -3px -5px 2px #D9D4CF;
  }

    table
    {
      margin: auto;
    }
    td
    {
      padding : 5px;
      text-align: center;
    }

    tr
    {
      padding: 5px;

    }

</style>



<div class="container">
  <div class="columns">
    <div class="column col-10 col-mx-auto">
      <div class="wrapper1" style="margin-top:20px;float:left;">
          <a id="a" href="view-routes.php" class="second after"> <<< Go Back  </a>
      </div>

      <div style="margin-bottom:200px;margin-top:70px;" class="simple-form">
        <form action="" id="registration" method="POST" class="form-group">
        <table cellpadding= 10px>
          <tr>
            <td>
              <label style="color:white;" class="form-label" for="picktime">PICKUP TIME</label>
            </td>
            <td>
              <input type="time" name="picktime" style="padding-left:15px;border-radius:5px;" value="<?php echo $busRoute['pickup_time']?>" required/>
            </td>
          </tr>
          <tr>
            <td>
              <label style="color:white" class="form-label" for="picktime">Pick up Location and Route</label>
            </td>
            <td>
              <select name="location" value="<?php echo $busRoute['pickup_location']?>" style="border-radius:5px;" class="form-select" required>
                  <option value="Bramp-Lamb">Brampton-Lambton</option>
                  <option value="Lamb-Bram">Lambton-Brampton</option>
                  <option value="Lamb-Missi">Lambton-Mississauga</option>
                  <option value="Missi-Lamb">Mississauga-Lambton</option>
              </select>
            </td>
          </tr>
          <tr>

            <td colspan=2>
              <label style="color:white;font-family: Helvetica georgia;font-weight:bold; font-size: 20px;" class="form-label" for="daysava"> DAYS AVAILABLE</label>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="mon">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Monday</label><br/>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="tue">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Tuesday</label><br/>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="wed">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Wednesday</label><br/>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="thu">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Thursday</label>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="fri">
            </td>
            <td>
                <label class="form-checkbox" style="color:white;">Friday</label>
            </td>
          </tr>
          <tr>
            <td>
                <input type="checkbox" name="check_list[]" value="sat">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Saturday</label>
            </td>
          </tr>
          <tr>
            <td>
              <input type="checkbox" name="check_list[]" value="sun">
            </td>
            <td>
              <label class="form-checkbox" style="color:white;">Sunday</label>
            </td>
          </tr>
          <tr>
            <td colspan=2>
              <input type="submit" value="Update Bus Route" class="btn btn-success"/>
            </td>
          </tr>
        </table>
      </form>
    </div>

    </div> <!--//col-10-->
  </div> <!--//columns -->
</div> <!--// container -->

    <?php
      include("footer.php");
    ?>
