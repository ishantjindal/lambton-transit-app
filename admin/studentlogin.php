<!doctype html>
<html>
  <head>
      <meta charset="utf-8">
      <title>Input Form UI Design</title>
      <style type="text/css">
      body
      {
        margin: 0;
        padding: 0;
        font-family: sans-serif;
        background: url("https://img00.deviantart.net/a3d2/i/2012/191/c/0/eurovision_turkey_fan_page_background_by_alisarikaya-d56oqze.jpg");
        background-repeat: no-repeat;
        background-size: 100% 850px;

      }

      .login-box
      {
        width: 320px;
        height: 420px;
        background: rgba(0,0,0,.5);
        color: #fff;
        top: 50%;
        left: 50%;
        position: absolute;
        transform: translate(-50%,-50%);
        box-sizing: border-box;
        padding: 70px 30px;
        box-shadow: 0 15px 25px rgba(0,0,0,.5);
        border-radius: 10px;
      }

      .avatar
      {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        position: absolute;
        top: -50px;
        left: calc(50% - 50px);
      }

      h1
      {
        margin: 0px;
        padding: 0 0 20px;
        text-align: center;
        font-size: 24px;
        color: #fff;

      }

      .login-box p
      {
        margin: 0;
        padding: 0;
        font-weight: bold;

      }

      .login-box input
      {
        width: 100%;
        margin-bottom: 20px;

      }

      .login-box input[type="text"], [type="password"]
      {
          border: none;
          border-bottom: 1px solid #fff;
          background: transparent;
          outline: none;
          height: 40px;
          color: #fff;
          font-size: 16px;
      }


      .login-box input[type="submit"]
      {
          border: none;
          outline: none;
          color: #fff;
          background: #03a9f4;
          height: 40px;
          font-size: 18px;
          cursor: pointer;
          border-radius: 20px;
          margin-top: 10px;
      }

      .login-box input[type="submit"]:hover
      {
        cursor: pointer;
        background: #ff8a00;
        color: #fff;
      }

      </style>
  </head>
  <body>
    <div class="login-box">
      <img src="http://droidlessons.com/wp-content/uploads/2017/05/person-1824144_960_720-e1494184045144.png" class="avatar">
      <h1>Login Here</h1>
      <form>
        <p>Username</p>
        <input type="text" name="username" placeholder="Enter Username" required>
        <p>Password</p>
        <input type="password" name="password" placeholder="Enter Password" required>
        <input type="submit" name="submit" value="Login">
      </form>
    </div>
  <!--https://cdn3.iconfinder.com/data/icons/flatastic-4-1/256/male_user_add-512.png-->
  </body>
</html>
