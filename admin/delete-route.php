<?php
  if (isset($_GET["id"]) == FALSE)
  {
    // missing an id parameters, so
    // redirect person back to add employee page
    header("Location: " . "view-routes.php");
    exit();
  }

  $id = $_GET["id"];

  // check for a POST request
  if ($_SERVER['REQUEST_METHOD'] == 'POST')
  {
    require("db_credentials.php");
    $connection = connect();
    deleteBusInformation();
  }
  else
  {
    echo " GET REQUEST";
  }
?>
<?php
  include("headr.php");
?>

<div class="top">
  <h1>DELETE BUS INFO</h1>
</div>
</header>
<style>
        .top
        {
           position: relative;
           text-align: center;
           color: #767676;
           top: 20px;
        }

        .top h1
        {
           font-size: 52px;
           margin: 25px;
           padding-bottom: 10px;
           font-weight: bold;
           opacity: 0.5;
           line-height: 120%;
           text-decoration: underline;
           text-shadow: -3px -5px 2px #D9D4CF;
        }

        .message
        {
          background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 100, 0.5)), url("https://i.stack.imgur.com/p9mUO.jpg");
          background-repeat: no-repeat;
          height: 10vh;
          background-size: cover;
          background-position: center;
          color: #fff;
          font-family: Baskerville, "Baskerville Old Face", "Hoefler Text", Garamond, "Times New Roman", serif;
          font-size: 30px;
          font-style: normal;
          font-variant: normal;
          line-height: 24.2px;
          font-weight: 700;
          padding: 25px;
          text-align: center;
          text-shadow: 0 15px 25px rgba(0,0,0,.5);
          border-radius: 10px;
          margin-top: 40px;
        }

        #btn_s
        {
            width: 100%;
            margin: auto 0;
            text-align: center;

        }

        .btn1
        {
           text-decoration: none;
           color: #133EB9;
           padding: 5px 10px;
           letter-spacing: 2px;
           border: 1px solid #133EB9;
           padding: 10px;
           width: 200px;
           margin-top: 20px;
           margin-left: auto;
           margin-right: auto;
           border-radius: 20px;
           font-weight: bold;
        }

        .btn1:hover
        {
          text-decoration: none;
          border: 1px solid #FF8500;
          background: #FF8500;
          transition: .2s;
          color: #fff;
        }

        .btn2
        {
           text-decoration: none;
           position: relative;
           color: #133EB9;
           padding: 5px 10px;
           letter-spacing: 2px;
           border: 1px solid #133EB9;
           padding: 10px;
           display: inline-block;
           width: 200px;
           text-align: center;
           margin-top: 20px;
           margin-left: 20px;
           border-radius: 20px;
           font-weight: bold;
        }

        .btn2:hover
        {
          text-decoration: none;
          border: 1px solid #FF8500;
          background: #FF8500;
          transition: .2s;
          color: #fff;

        }

</style>

<div class="container">
  <div class = "columns">
    <div class="column col-10 col-mx-auto">

  <!-- @TODO: Ask user to confirm they want to get delete a person! -->
  <div class="message"> ARE YOU SURE YOU WANNA DELETE? </div>
  <form action = "<?php echo "delete-route.php?id=" . $id; ?>" method ="POST">
    <div id="btn_s"><button class="btn1" type="submit" name="choice"> Yes </button>
    <a class="btn2" href="view-routes.php">No</a></div>
  </form>
    </div> <!--// col-12 -->
  </div> <!-- // column -->
</div> <!--// container -->
<?php
  include("footer.php");
?>
