<?php

	define("DBHOST", "localhost");
	define("DBUSER", "root");
	define("DBPASS", "");
	define("DBNAME", "buses");

	function connect()
	{
		 // 1. Create a database connection
		 $connection = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);

		 // show an error message if PHP cannot connect to the database
		 if (mysqli_connect_errno())
		 {
		    echo "Failed to connect to MySQL: " . mysqli_connect_error();
		    exit();
		 }

		 return $connection;
	}

	function getStudentsInformation()
	{
		 global $connection;
     $sql = "SELECT * FROM student";
		 $results = mysqli_query($connection, $sql);
		 if ($results == FALSE)
		 {
		    echo "Database query failed. <br/>";
		    echo "SQL command: " . $sql;
		    exit();
		 }
		 return $results;
	}

	function insertStudentInformation()
	{
		global $connection;
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
		  // Get the form values that were sent by addEmployee.php
		  $name = $_POST['Name'];
		  $mobile = $_POST['MobileNo'];
		  $user = $_POST['UserName'];
			$pass = $_POST['PassWord'];

			// @TODO: your database code should  here
			//---------------------------------------------------

			$sqla = "select * from student where username = '$user'";
			$result = mysqli_query($connection, $sqla);
			$num_rows = mysqli_num_rows($result);

			if($num_rows == 1)
			{

			}
			else
			{
				  // 2. Perform Insert query
				  $sql  = "INSERT INTO student ";
				  $sql .= "(student_name, mobile_no, username, password) ";
				  $sql .= "VALUES ";
				  $sql .= "(";
				  $sql .= "'" . $name ."', ";
				  $sql .= "'" . $mobile ."', ";
					$sql .= "'" . $user ."', ";
				  $sql .= "'" . $pass ."'";
				  $sql .= ")";

				  // run the sql query
				  $results = mysqli_query($connection, $sql);


				  if ($results == TRUE)
				  {
				    echo "<h1>Success</h1>";
				  }
				  else
				  {
				    echo "Database query failed. <br/>";
				    echo "SQL command: " . $query;
				    exit();
				  }
				}
}

		else
		{
		  // you got a GET request, so
		  // redirect person back to add employee page
		  header("Location: " . "addNewStudent.php");
		  exit();
		}
	}


	function showStudents()
	{
		   global $connection;
			 $id = $_GET["id"];
			 $sql = "Select * from student where id = '$id'";
			 $results = mysqli_query($connection, $sql);

			 if (mysqli_connect_errno())
			 {
				 echo "Failed to connect to MySQL: " . mysqli_connect_error();
				 exit();
			 }
			 // 2. Perform database query
			 if ($results == FALSE)
			 {
				 echo "Database query failed. <br/>";
				 echo "SQL command: " . $sql;
				 exit();
			 }
			 return $results;
	}

	function updateStudentInformation()
	{
			global $connection;
			$id = $_GET["id"];

			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				// Get the form values that were sent by addEmployee.php
				$name = $_POST['Name'];
			  $mobile = $_POST['MobileNo'];
			  $user = $_POST['UserName'];
				$pass = $_POST['PassWord'];

				// @TODO: your database code should  here
				//---------------------------------------------------

				// 2. Perform Insert query
				$sql 	 = "UPDATE student SET ";
				$sql 	.= "student_name='" . $name . "', ";
				$sql 	.= "mobile_no='" . $mobile . "', ";
				$sql 	.= "username='" . $user . "', ";
				$sql 	.= "password='" . $pass . "' ";
				$sql 	.= "WHERE id='" . $id . "' ";
				$sql  .= "LIMIT 1";

				// run the sql query
				$result = mysqli_query($connection, $sql);

				if ($result == TRUE)
				{
					echo "<h1>Success</h1>";
					header("Location: " . "adminstudent.php");
				}
				else
				{
					echo "Database query failed. <br/>";
					echo "SQL command: " . $sql;
					exit();

				}

			}
	}

	function deleteStudentInformation()
	{
		 global $connection;
		 $id = $_GET["id"];
		 $sql 	 = "DELETE FROM student ";
		 $sql 	.= "WHERE id= '" . $id . "'";
		 $sql 	.= "LIMIT 1";

		 $results = mysqli_query($connection, $sql);

		 if ($results == FALSE)
		 {
				echo "Database query failed. <br/>";
				echo "SQL command: " . $sql;
				exit();
		 }
		 else
		 {
			 //$url = "adminstudent.php";
			 //echo "<script>window.open('$url')</script>";
			 //echo "<script>window.close()</script>";
			 header("Location: adminstudent.php");
			 exit();

		 }
	}




	function getBusesInformation()
	{
		 global $connection;
     $sql = "SELECT * FROM buses";
		 $results = mysqli_query($connection, $sql);
		 if ($results == FALSE)
		 {
		    echo "Database query failed. <br/>";
		    echo "SQL command: " . $sql;
		    exit();
		 }
		 return $results;
	}


	function insertBusesInformation()
	{
		global $connection;
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
				$pickupTime = $_POST['picktime'];
			  $location = $_POST['location'];
			  $monday = 0;
			  $tuesday = 0;
			  $wednesday = 0;
			  $thursday = 0;
			  $friday = 0;
			  $saturday = 0;
			  $sunday = 0;

			  foreach($_POST['check_list'] as $i)
			  {
			      if($i == "mon")
			      {
			          $monday = 1;
			      }

			      if($i == "tue")
			      {
			          $tuesday = 1;
			      }

			      if($i == "wed")
			      {
			          $wednesday = 1;
			      }

			      if($i == "thu")
			      {
			          $thursday = 1;
			      }
			      if($i == "fri")
			      {
			          $friday = 1;
			      }

			      if($i == "sat")
			      {
			          $saturday = 1;
			      }

			      if($i == "sun")
			      {
			          $sunday = 1;
			      }
			  }

				// 2. Perform Insert query
				$sql =  "INSERT INTO buses";
	 			$sql .= " (pickup_time, pickup_location, Mon, Tue, Wed, Thu, Fri, Sat, Sun)";
	 			$sql .= " VALUES (";
	 			$sql .= "'" . $pickupTime . "',";
	 			$sql .= "'" . $location . "',";
	 			$sql .= "'" . $monday . "',";
	 			$sql .= "'" . $tuesday . "',";
	 			$sql .= "'" . $wednesday . "',";
	 			$sql .= "'" . $thursday . "',";
	 			$sql .= "'" . $friday . "',";
	 			$sql .= "'" . $saturday . "',";
	 			$sql .= "'" . $sunday . "'";
	 			$sql .=")";

				// run the sql query
				$results = mysqli_query($connection, $sql);

				if ($result == TRUE)
				{
						$new_id = mysqli_insert_id($connection);
						header("Location: " . "view-routes.php");
						exit();
				}
				else
				{
						echo "INSERT failed. <br/>";
						echo "SQL command: " . $sql;

						// print out a the error
						echo mysqli_error($connection);
						mysqli_close($connection);
						exit();
	      }
				mysqli_free_result($results);

   	}

	 	else
	 	{
		 		// you got a GET request, so
				// redirect person back to add employee page
				header("Location: " . "createRoute.php");
				exit();
		}
}

function showBuses()
{
		 global $connection;
		 $id = $_GET["id"];
		 $sql =  "SELECT * from buses";
	   $sql .= " WHERE id='" . $id . "'";
		 $results = mysqli_query($connection, $sql);


		 if (mysqli_connect_errno())
		 {
			 echo "Failed to connect to MySQL: " . mysqli_connect_error();
			 exit();
		 }
		 // 2. Perform database query
		 if ($results == FALSE)
		 {
			 echo "Database query failed. <br/>";
			 echo "SQL command: " . $sql;
			 exit();
		 }
		 return $results;
}


function updateBusInformation()
{
	global $connection;

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
			$busRoute = [];
	    $busRoute["bus_time"] = $_POST['picktime'];
	    $busRoute["route"] = $_POST['location'];

			$monday = 0;
			$tuesday = 0;
			$wednesday = 0;
			$thursday = 0;
			$friday = 0;
			$saturday = 0;
			$sunday = 0;

			foreach($_POST['check_list'] as $i)
			{
					if($i == "mon")
					{
							$monday = 1;
					}

					if($i == "tue")
					{
							$tuesday = 1;
					}

					if($i == "wed")
					{
							$wednesday = 1;
					}

					if($i == "thu")
					{
							$thursday = 1;
					}
					if($i == "fri")
					{
							$friday = 1;
					}

					if($i == "sat")
					{
							$saturday = 1;
					}

					if($i == "sun")
					{
							$sunday = 1;
					}
			}

			$busRoute["Mon"] = $monday;
			$busRoute["Tue"] = $tuesday;
			$busRoute["Wed"] = $wednesday;
			$busRoute["Thu"] = $thursday;
			$busRoute["Fri"] = $friday;
			$busRoute["Sat"] = $saturday;
			$busRoute["Sun"] = $sunday;


			// 2. Perform Insert query
			$query = "UPDATE buses SET  ";
	    $query .= "pickup_time='" . $busRoute["bus_time"] . "', ";
	    $query .= "pickup_location='" . $busRoute["route"] . "', ";
	    $query .= "Mon='" . $busRoute["Mon"] . "', ";
	    $query .= "Tue='" . $busRoute["Tue"] . "', ";
	    $query .= "Wed='" . $busRoute["Wed"] . "', ";
	    $query .= "Thu='" . $busRoute["Thu"] . "', ";
	    $query .= "Fri='" . $busRoute["Fri"] . "', ";
	    $query .= "Sat='" . $busRoute["Sat"] . "', ";
	    $query .= "Sun='" . $busRoute["Sun"] . "' ";
	    $query .= " WHERE id='" . $id . "'";
	    $query .= " LIMIT 1";

	    $result = mysqli_query($connection, $query);

			if ($result == TRUE)
			{
				  header("Location: view-routes.php");
					exit();
			}
			if ($result == FALSE)
			{
				echo "Database query failed. <br/>";
				echo "SQL command: " . $query;
				exit();
			}
			mysqli_free_result($result);
	    mysqli_close($connection);
 }
}

function deleteBusesInformation()
{
	 global $connection;
	 $id = $_GET["id"];
	 $sql  = "DELETE FROM buses ";
	 $sql .= "WHERE id='" . $id . "' ";
	 $sql .= "LIMIT 1";

	 $results = mysqli_query($connection, $sql);

	 if ($results == FALSE)
	 {
		 echo "Database query failed. <br/>";
		 echo "SQL command: " . $query;
		 exit();
	 }
	 else
	 {
		 header("Location: " . "view-routes.php");
		 exit();
	 }
}
?>
