<?php require_once("db_credentials.php") ?>
<?php

// 0. Tell PHP you are going to return a JSON
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// 1. Create a database connection
$connection = connect();

// show an error message if PHP cannot connect to the database
if (!$connection)
{
  $output = array("error" => "Failed to connect to MySQL: " . mysqli_connect_error());
  echo json_encode($output);
  exit();
}

// 2. Perform database query
$sql = "SELECT * FROM buses";

$results = mysqli_query($connection, $sql);
if ($results == FALSE) {
  $output = array("error" => "Database quer failed. SQL command: " . $sql);
  echo json_encode($output);
  exit();
}

$output = [];

while ($row = mysqli_fetch_assoc($results)) {
  $routes = array(
    "id" => $row["id"],
    "pickup_time" => $row["pickup_time"],
    "pickup_location" => $row["pickup_location"],
    "Mon" => $row["Mon"],
    "Tue" => $row["Tue"],
    "Wed" => $row["Wed"],
    "Thu" => $row["Thu"],
    "Fri" => $row["Fri"],
    "Sat" => $row["Sat"],
    "Sun" => $row["Sun"]
  );
  array_push($output, $routes);
}

echo json_encode($output);


?>
