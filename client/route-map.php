<?php
  include("headr.php");
?>
<div class="top">
  <h1>ROUTE MAP</h1>
</div>
</header>
<style>

  .top
  {
     position: relative;
     text-align: center;
     color: #767676;
     top: 20px;
  }

  .top h1
  {
     font-size: 52px;
     margin: 25px;
     padding-bottom: 10px;
     font-weight: bold;
     opacity: 0.5;
     line-height: 120%;
     text-decoration: underline;
     text-shadow: -3px -5px 2px #D9D4CF;
  }
  </style>
  <div class="container">
    <div class="columns">
      <div class="column col-10 col-mx-auto">
          <h1 style="text-align:center;"> Lambton to Brampton or Brampton to Lambton</h1>
        	<div style="height:500px; width:700px;margin:auto; ">
        	     <iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d112907.13608804534!2d-79.63807646050023!3d43.741480533353375!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e0!4m5!1s0x882b2d6fc6ca7f5d%3A0x2a12502c1cd1e815!2sLambton+College%2C+Yorkland+Boulevard%2C+North+York%2C+ON!3m2!1d43.7732574!2d-79.3358993!4m5!1s0x882b1565e04fb323%3A0x6a2c8a93b5374493!2sCharolais+Blvd%2C+Brampton%2C+ON!3m2!1d43.659234399999995!2d-79.7502695!5e0!3m2!1sen!2sca!4v1518717309563" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        	</div>
	        <h1 style="text-align:center;"> Lambton to Mississauga or Mississauga to Lambton </h1>
      		<div style="height:500px; width:700px;margin:auto;margin-bottom:100px;">
        		<iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d184556.50699831668!2d-79.65811797544609!3d43.716878364828524!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e0!4m5!1s0x882b2d6fc6ca7f5d%3A0x2a12502c1cd1e815!2sLambton+College%2C+Yorkland+Boulevard%2C+North+York%2C+ON!3m2!1d43.7732574!2d-79.3358993!4m5!1s0x882b472cf79352c7%3A0x514e1dcb762424b2!2sSquare+One%2C+Mississauga%2C+ON!3m2!1d43.5946693!2d-79.6474915!5e0!3m2!1sen!2sca!4v1518717629857" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>

       </div> <!--//col-10-->
     </div> <!--//columns -->
    </div> <!--// container -->
    <?php
     include("footer.php");
    ?>
