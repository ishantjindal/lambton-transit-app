<?php

	define("DBHOST", "localhost");
	define("DBUSER", "root");
	define("DBPASS", "");
	define("DBNAME", "buses");

	function connect()
	{
		 // 1. Create a database connection
		 $connection = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);

		 // show an error message if PHP cannot connect to the database
		 if (mysqli_connect_errno())
		 {
		    echo "Failed to connect to MySQL: " . mysqli_connect_error();
		    exit();
		 }

		 return $connection;
	}

	function getStudentsInformation()
	{
		 global $connection;
     $sql = "SELECT * FROM student";
		 $results = mysqli_query($connection, $sql);
		 if ($results == FALSE)
		 {
		    echo "Database query failed. <br/>";
		    echo "SQL command: " . $sql;
		    exit();
		 }
		 return $results;
	}

	function insertStudentInformation()
	{
		global $connection;
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
		  // Get the form values that were sent by addEmployee.php
		  $name = $_POST['Name'];
		  $mobile = $_POST['MobileNo'];
		  $user = $_POST['UserName'];
			$pass = $_POST['PassWord'];

		  // @TODO: your database code should  here
		  //---------------------------------------------------

		  // 2. Perform Insert query
		  $sql  = "INSERT INTO student ";
		  $sql .= "(student_name, mobile_no, username, password) ";
		  $sql .= "VALUES ";
		  $sql .= "(";
		  $sql .= "'" . $name ."', ";
		  $sql .= "'" . $mobile ."', ";
			$sql .= "'" . $user ."', ";
		  $sql .= "'" . $pass ."'";
		  $sql .= ")";

		  // run the sql query
		  $results = mysqli_query($connection, $sql);


		  if ($results == TRUE)
		  {
		    echo "<h1>Success</h1>";
		  }
		  else
		  {
		    echo "Database query failed. <br/>";
		    echo "SQL command: " . $query;
		    exit();
		  }
		  //---------------------------------------------------

		  // @TODO: delete these two statement after your add your db code
		  echo "I got a POST request! <br />";
		}
		else
		{
		  // you got a GET request, so
		  // redirect person back to add employee page
		  header("Location: " . "addNewStudent.php");
		  exit();
		}
	}


	function showStudents()
	{
		   global $connection;
			 $id = $_GET["id"];
			 $sql = "Select * from student where id = '$id'";
			 $results = mysqli_query($connection, $sql);

			 if (mysqli_connect_errno())
			 {
				 echo "Failed to connect to MySQL: " . mysqli_connect_error();
				 exit();
			 }
			 // 2. Perform database query
			 if ($results == FALSE)
			 {
				 echo "Database query failed. <br/>";
				 echo "SQL command: " . $sql;
				 exit();
			 }
			 return $results;
	}

	function updateStudentInformation()
	{
			global $connection;
			$id = $_GET["id"];

			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				// Get the form values that were sent by addEmployee.php
				$name = $_POST['Name'];
			  $mobile = $_POST['MobileNo'];
			  $user = $_POST['UserName'];
				$pass = $_POST['PassWord'];

				// @TODO: your database code should  here
				//---------------------------------------------------

				// 2. Perform Insert query
				$sql 	 = "UPDATE student SET ";
				$sql 	.= "student_name='" . $name . "', ";
				$sql 	.= "mobile_no='" . $mobile . "', ";
				$sql 	.= "username='" . $user . "', ";
				$sql 	.= "password='" . $pass . "' ";
				$sql 	.= "WHERE id='" . $id . "' ";
				$sql  .= "LIMIT 1";

				// run the sql query
				$result = mysqli_query($connection, $sql);

				if ($result == TRUE)
				{
					echo "<h1>Success</h1>";
					header("Location: " . "adminstudent.php");
				}
				else
				{
					echo "Database query failed. <br/>";
					echo "SQL command: " . $sql;
					exit();

				}

			}
	}
?>
