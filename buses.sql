-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2018 at 05:36 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buses`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('lambton@cestar.com', 'Cestar@1234');

-- --------------------------------------------------------

--
-- Table structure for table `buses`
--

CREATE TABLE `buses` (
  `id` int(11) NOT NULL,
  `pickup_time` time DEFAULT '00:00:00',
  `pickup_location` varchar(100) NOT NULL,
  `mon` tinyint(1) NOT NULL,
  `tue` tinyint(1) NOT NULL,
  `wed` tinyint(1) NOT NULL,
  `thu` tinyint(1) NOT NULL,
  `fri` tinyint(1) NOT NULL,
  `sat` tinyint(1) NOT NULL,
  `sun` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buses`
--

INSERT INTO `buses` (`id`, `pickup_time`, `pickup_location`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`, `sun`) VALUES
(1, '06:45:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 0, 0),
(2, '06:45:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 0, 0),
(3, '07:00:00', 'Brampton-Lambton', 0, 0, 0, 0, 0, 1, 0),
(4, '06:45:00', 'Mississauga-Lambton', 1, 1, 1, 1, 1, 0, 0),
(5, '07:15:00', 'Mississauga-Lambton', 0, 0, 0, 0, 0, 1, 0),
(6, '10:20:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 0, 0),
(7, '11:00:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 1, 0),
(8, '11:15:00', 'Mississauga-Lambton', 1, 1, 1, 1, 1, 0, 0),
(9, '11:40:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 1, 0),
(10, '12:30:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 1, 0),
(11, '12:00:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 1, 0),
(12, '14:30:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 0, 0),
(13, '15:40:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 0, 0),
(14, '16:00:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 1, 0),
(15, '16:25:00', 'Brampton-Lambton', 1, 1, 1, 1, 1, 0, 0),
(16, '16:25:00', 'Mississauga-Lambton', 1, 1, 1, 1, 1, 0, 0),
(17, '11:30:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(18, '12:30:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(19, '12:30:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 1, 0),
(20, '13:30:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(21, '13:45:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(22, '13:45:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 1, 0),
(23, '14:30:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 0, 0),
(24, '14:30:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 0, 0),
(25, '15:30:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 0, 0),
(26, '15:30:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 0, 0),
(27, '16:00:00', 'Lambton-Brampton', 0, 0, 0, 0, 0, 1, 0),
(28, '16:00:00', 'Lambton-Mississauga', 0, 0, 0, 0, 0, 1, 0),
(29, '17:00:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(30, '17:00:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 1, 0),
(31, '17:05:00', 'Lambton-Brampton', 1, 0, 1, 0, 1, 0, 0),
(32, '18:00:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(33, '18:00:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 1, 0),
(34, '19:30:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(35, '19:30:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 1, 0),
(36, '19:45:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(37, '20:10:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(38, '20:10:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 1, 0),
(39, '21:35:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 0, 0),
(40, '21:45:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 0, 0),
(41, '21:45:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 0, 0),
(42, '22:20:00', 'Lambton-Brampton', 1, 1, 1, 1, 1, 1, 0),
(43, '22:20:00', 'Lambton-Mississauga', 1, 1, 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `student_name` varchar(200) NOT NULL,
  `mobile_no` varchar(12) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `student_name`, `mobile_no`, `username`, `password`) VALUES
(39, 'Vikram', '647-900-2200', 'vikram@gmail.com', 'Cestar%1234'),
(40, 'Ishant Jindal', '647-761-9542', 'ishantjindal1992@gmail.com', 'College@1234');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
